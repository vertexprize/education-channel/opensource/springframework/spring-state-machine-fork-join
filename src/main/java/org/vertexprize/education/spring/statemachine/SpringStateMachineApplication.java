package org.vertexprize.education.spring.statemachine;

import jakarta.annotation.PostConstruct;
import jakarta.annotation.PreDestroy;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import reactor.core.publisher.Mono;

@SpringBootApplication
@Slf4j
public class SpringStateMachineApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringStateMachineApplication.class, args);
    }

    @Autowired
    private StateMachine<String, String> stateMachine;

    @PostConstruct
    public void start() {

        log.info("Начальное состояние машины: " + stateMachine.getInitialState().getId());               
        
        String initBody = "init";  
        log.info("Событие инициализации: " +initBody);
        Message<String> initMsg = MessageBuilder.withPayload(initBody).build();
        

        log.info("Старт машины состостояний: " + stateMachine.getId());
        stateMachine.getStateMachineAccessor()
                .doWithAllRegions( 
                        s->
                        {
                            s.setInitialEnabled(true);    
                            s.setForwardedInitialEvent(initMsg);                            
                        });
        
        Mono<Void> startReactively = stateMachine.startReactively();
        startReactively.subscribe();
        log.info("Машина состояний запущена: " + stateMachine.getId());                 
        List<String> states = stateMachine.getStates().stream().map( s -> s.getId()).collect(Collectors.toList());
        List<String> transitions = stateMachine.getTransitions().stream().map( t -> t.getName()).collect(Collectors.toList());        
        
        log.info(String.format("%-35s %s", "Список состояний [] записей:",states));                
        log.info(String.format("%-35s %s", "Список переходов [] записей:", transitions));
        
        
        
        String event = "E1";                                       
        log.info("Отправка события [" + event + "] ... ");       
        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
        
        
        event = "E1";                                       
        log.info("Отправка события [" + event + "] ... ");       
        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
        
        
        
//        event = "E3";
//        log.info("Отправка события [" + event + "] ... ");      
//        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
//        
//        
//        
//        event = "E2";
//        log.info("Отправка события [" + event + "] ... ");      
//        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
//       
//        
//        
//      
//        event = "E4";
//        log.info("Отправка события [" + event + "] ... ");      
//        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
//        
//        event = "E5";
//        log.info("Отправка события [" + event + "] ... ");      
//        stateMachine.sendEvent(Mono.just(MessageBuilder.withPayload(event).build())).subscribe();
//       
        
     
    }

    @PreDestroy
    public void stop() {
        log.info("Остановка  машины состостояний ...");
        Mono<Void> startReactively = stateMachine.stopReactively();
        startReactively.subscribe();

    }

}
