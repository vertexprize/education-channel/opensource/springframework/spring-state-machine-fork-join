/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.statemachine.config;

import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.StateContext;
import org.springframework.statemachine.action.Action;
import org.springframework.statemachine.config.EnableStateMachine;

import org.springframework.statemachine.config.StateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineModelConfigurer;
import org.springframework.statemachine.config.model.DefaultStateMachineComponentResolver;
import org.springframework.statemachine.config.model.StateMachineComponentResolver;
import org.springframework.statemachine.config.model.StateMachineModelFactory;
import org.springframework.statemachine.guard.Guard;
import org.springframework.statemachine.uml.UmlStateMachineModelFactory;

/**
 * Конфигурация машины состояний
 *
 * @author vaganovdv
 */
@Configuration
@EnableStateMachine
@Slf4j
public class StateMachieConfig extends StateMachineConfigurerAdapter<String, String> {

//  Для сохранения машины состояний в БД    
//    @Autowired
//    private StateRepository<? extends RepositoryState> stateRepository;
//
//    @Autowired
//    private TransitionRepository<? extends RepositoryTransition> transitionRepository;
    @Autowired
    private StateMachineLogListener stateMachineLogListener;

    @Override
    public void configure(StateMachineModelConfigurer<String, String> model) throws Exception {
        model.withModel()
                .factory(modelFactory());
    }

    @Override
    public void configure(StateMachineConfigurationConfigurer<String, String> config)
            throws Exception {
        config
                .withConfiguration()
                .autoStartup(false)
                .machineId("state-machine")
                .listener(stateMachineLogListener);
    }

    /**
     * Для создания машины состояний из БД
     *
     * @return
     */
    /*
    @Bean
    public StateMachineModelFactory<String, String> modelFactory() {
		return new RepositoryStateMachineModelFactory(stateRepository, transitionRepository);
    }
     */
    @Bean
    public StateMachineModelFactory<String, String> modelFactory() {
       return new UmlStateMachineModelFactory("classpath:/models/spring-sm-fork-join/spring-sm-fork-join.uml");
    }

    @Bean
    public StateMachineComponentResolver<String, String> stateMachineComponentResolver() {
        DefaultStateMachineComponentResolver<String, String> resolver = new DefaultStateMachineComponentResolver<>();
        resolver.registerAction("sendMessage", sendMessage());
        return resolver;
    }

    @Bean
    public Action<String, String> sendMessage() {
        return new Action<String, String>() {
            @Override
            public void execute(StateContext<String, String> context) {                
                context.getStateMachine().getState().getId();
                
                log.info("ACTION ===>  Выполнение действия [sendMessage],  событие: " + context.getEvent() + ", состяние: " + context.getTarget().getId());
            }
        };
    }

    
    
    /**
     * Действие в состоянии PaymentState
     * @return 
     */
    @Bean
    public Action<String, String> entryPaymentState() {
       return (context) -> {
            String state = "";
            if (context.getStateMachine().getState().getId() != null && !context.getStateMachine().getState().getId().isEmpty()) {
                state = context.getStateMachine().getState().getId();
            }
            log.info(String.format("%-55s %s", "[" + context.getStateMachine().getId() + "]", "ДЕЙСТВИЕ: вход в состояние: "+state));
        };
    }
    
    /**
     * Действие в состоянии PaymentState
     *
     * @return
     */
    @Bean
    public Action<String, String> entryPrepareOrder() {
        return (context) -> {
            String state = "";
            if (context.getStateMachine().getState().getId() != null && !context.getStateMachine().getState().getId().isEmpty()) {
                state = context.getStateMachine().getState().getId();
            }
            log.info(String.format("%-55s %s", "[" + context.getStateMachine().getId() + "]", "ДЕЙСТВИЕ: вход в состояние: "+state));
        };
    }
    

    /**
     * Использутеся для организации выбора Choice Устанавливется на переход
     *
     * @return
     */
    @Bean
    public Guard<String, String> orderOk() {
        return (context) -> {
            Map<Object, Object> variables = context.getExtendedState().getVariables();
            String event = context.getEvent();
            String source = "";
            String target = "";
            
            if (context.getSource().getId() != null && !context.getSource().getId().isEmpty()) {
                source = context.getSource().getId();
            }
            if (context.getTarget().getId() != null && !context.getTarget().getId().isEmpty()) {
                target = context.getTarget().getId();
            }

            boolean result;
            if (variables.containsKey("customer") && variables.containsKey("order")) {
                result = true;
            } else {
                result = false;
            }
            log.info(String.format("%-55s %s", "[" + context.getStateMachine().getId() + "]", "ЗАЩИТНИК перехода [" + source + "] ==> [" + target + "] результата: " + result));
            return result;

        };
    }

}
