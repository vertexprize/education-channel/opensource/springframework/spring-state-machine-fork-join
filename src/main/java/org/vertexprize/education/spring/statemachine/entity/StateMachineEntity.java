/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.statemachine.entity;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author vaganovdv
 */
@Data
@NoArgsConstructor
@Getter
@Setter
public class StateMachineEntity {
    
    private String event;
    private String statemachineId;
    private String source;
    private String taget;
    private String status;
    
}
