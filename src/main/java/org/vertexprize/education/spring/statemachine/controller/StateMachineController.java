/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.vertexprize.education.spring.statemachine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.vertexprize.education.spring.statemachine.entity.StateMachineEntity;

/**
 *
 * @author vaganovdv
 */
@Controller
public class StateMachineController {

   
    
    @Autowired
    private StateMachine<String, String> stateMachine;



    /**
     * Получение состояния машины
     *
     * @param machineId
     * @return
     */
    
    
    @RequestMapping(value = "/start", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> start(@PathVariable("id") String machineId) {
        
        StateMachineEntity entity = new StateMachineEntity();
        
        if (!stateMachine.isComplete())
        {
            stateMachine.startReactively().block();
        }   
        
        
        
        return ResponseEntity.ok(entity);

    }
    

    
    
    
    
    
//    private synchronized StateMachine<String, String> getStateMachine(String machineId) throws Exception {
//		
//		if (currentStateMachine == null) {
//			currentStateMachine = stateMachineService.acquireStateMachine(machineId);
//			currentStateMachine.addStateListener(listener);
//			currentStateMachine.startReactively().block();
//		} else if (!ObjectUtils.nullSafeEquals(currentStateMachine.getId(), machineId)) {
//			stateMachineService.releaseStateMachine(currentStateMachine.getId());
//			currentStateMachine.stopReactively().block();
//			currentStateMachine = stateMachineService.acquireStateMachine(machineId);
//			currentStateMachine.addStateListener(listener);
//			currentStateMachine.startReactively().block();
//		}
//		return currentStateMachine;
//	}
    
}
